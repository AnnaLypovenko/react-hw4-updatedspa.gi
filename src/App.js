import React from 'react';
import './App.css';
import Button from "./components/Button/Button";

import { Route, Switch } from 'react-router-dom';
import Products from './pages/Products/Products';
import Cart from './pages/Cart/Cart';
import Favorites from './pages/Favorites/Favorites';
import ReduxForm from './forms/ReduxForm'
import * as actions from "./store/actions/productsActions";
import Header from "./components/Header/Header";
import Body from "./components/Body/Body";


function App() {
  return (
    <div className="App">
        <Header/>
      <Switch>
        <Route exact path="/" component={Products} />
        <Route exact path="/cart" component={Cart} />
        <Route exact path="/favorites" component={Favorites} />
      </Switch>
        <Body/>

    </div>
  );
}

// const mapStateToProps = (state) => {
//     return {
//         product: state.products.product,
//         productsArray: state.products.productsArray,
//         favoriteArray: state.products.favoriteArray,
//         cartArray: state.products.cartArray,
//     };
// };

export default App;
