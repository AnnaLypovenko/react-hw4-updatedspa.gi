import React from "react";
import {Field, reduxForm} from "redux-form";
import MyInput from "../components/MyInput/MyInput"
import form from "./form.scss"

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const name = (input) => !input && "Your name";
const surname = (input) => !input && "Your surname";
const age = (input) => !input && "Your age";
const telephone = (input) => !input && "Your telephone";
const address = (input) => !input && "Your address";

const requiredInput = (input) => !input && "Field is required";
const isEmail = (input) => !EMAIL_REGEX.test(input) && "Please enter a valid email";
const passwordMatch = (input, values) => input !== values.password && "Password doesn't match";
const validate = values => {
    const {name, surname, age, telephone, address, login, password, confirmPassword} = values;
    const validationErrors = {};

    if (!EMAIL_REGEX.test(login)) {
        validationErrors.login = "This is not a valid email";
    }

    if (!login) {
        validationErrors.login = "This field is required";
    }

    if (!name) {
        validationErrors.login = "This field is required";
    }

    if (!surname) {
        validationErrors.login = "This field is required";
    }
    if (!age) {
        validationErrors.login = "This field is required";
    }

    if (!telephone) {
        validationErrors.login = "This field is required";
    }

    if (!address) {
        validationErrors.login = "This field is required";
    }

    if (!password) {
        validationErrors.password = "This field is required";
    }

    if (!confirmPassword) {
        validationErrors.confirmPassword = "This field is required";
    }

    if (password !== confirmPassword) {
        validationErrors.confirmPassword = "Password does not match";
    }
    return validationErrors;
};

let ReduxForm = (props) => {

    return (
        <div>
            <form className="form" onSubmit={props.handleSubmit}>
                <div className="form__header">SHOPPING BAG</div>
                <Field className="input" component={MyInput} type='text' name='login' placeholder='Login'
                       validate={[requiredInput, isEmail]}/>
                <Field className="input" component={MyInput} type='text' name='name' placeholder='Name'
                       validate={[requiredInput]}/>
                <Field className="input" component={MyInput} type='text' name='surname' placeholder='Surname'
                       validate={[requiredInput]}/>
                <Field component={MyInput} type='number' name='age' placeholder='Age'
                       validate={[requiredInput]}/>
                <Field className="input" component={MyInput} type='tel' name='telephone' placeholder='Telephone'
                       validate={[requiredInput]}/>
                <Field className="input" component={MyInput} type='text' name='address' placeholder='Address'
                       validate={[requiredInput]}/>
                <Field className="input" component={MyInput} type='password' name='password' placeholder='Password'
                       validate={[requiredInput]}/>
                <Field className="input" component={MyInput} type='password' name='confirmPassword' placeholder='Confirm password'
                       validate={[requiredInput, passwordMatch]}/>
                <div>
                    <button type='button' onClick={() => props.reset()}>Reset</button>
                </div>
                <div>
                    <button type='submit'>Checkout</button>
                </div>
            </form>
        </div>
    )
};
ReduxForm = reduxForm ({
    form: "login"
})(ReduxForm);

export default ReduxForm;