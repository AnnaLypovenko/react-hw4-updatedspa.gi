import Store from 'store';


const initialState = {
    product: null,
    productsArray: [],
    favoriteArray: [],
    cartArray: []
};
const approveActionFavorite = (productsArray, product) => {
    return (...favoriteArray) => {
        const idx = productsArray.indexOf(product);
        if (idx < 0) {
            Store.set('favorite_items', [...productsArray, product]);
            return [...productsArray, product];
        } else {
            const favoriteArray = [
                ...productsArray.slice(0, idx),
                ...productsArray.slice(idx + 1),
            ];
            Store.set('favorite_items', favoriteArray);
            return favoriteArray;
        }
    };
};
const approveActionCart = (productsArray, product) => {
    return (...cartArray) => {
        const idx = productsArray.indexOf(product);
        if (idx < 0) {
            Store.set('cart_items', [...productsArray, product]);
            return [...productsArray, product];
        } else {
            const cartArray = [
                ...productsArray.slice(0, idx),
                ...productsArray.slice(idx + 1),
            ];
            Store.set('cart_items', cartArray);
            return cartArray;
        }
    };
};

const productsReducer = (state = initialState, action) => {
    console.log('action type', action.type);
    switch (action.type) {
        case 'GET_PRODUCTS':
            return {
                ...state,
                product: getProducts.payload
            };

        case 'SET_PRODUCT_TO_FAVORITES':
            return {
                ...state,
                productsArray: action.payload,
                favoriteArray: approveActionFavorite(
                    state.favoriteArray,
                    state.product
                ),
            };

        case 'SET_PRODUCTS_TO_CART':
            return {
                ...state,
                productsArray: action.payload,
                cartArray: approveActionCart(
                    state.cartArray,
                    state.product),
            };

        default:
            return state;
    }
};

export default productsReducer;