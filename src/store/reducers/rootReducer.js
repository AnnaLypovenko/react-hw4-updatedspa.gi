import {combineReducers} from "redux";
import productsReducer from "../reducers/productsReducer";
import {reducer as formReducer} from 'redux-form';

const rootReducer = combineReducers(
    {
        products: productsReducer,
        form: formReducer
    });
export default rootReducer;