
export const getProducts =  async () => {
    const _fileName = "./data.json";
    const res = await fetch(`${_fileName}`);
    const body = await res.json();
    return body
};

export const setToFavorites = (id) => ({
    type:  setToFavorites(),
    payload: id,
});

export const setToCart = (id) => ({
    type: setToCart(),
    payload: id
});