import React from 'react';
import './Product.scss';

function Product ({product, productsActions}) {
    const {article, url, name, price, isInCart, isFavorite, onAddToCart, onAddToFavorite} = product;
    return (
        <div className={"product"}>
            <div>Name = {name}</div>
            <div>Article = {article}</div>
            <img className="product-image" src={url} alt="product image"/>
            <div>Price = {price}</div>
            <div>isFavorite = {isFavorite}</div>
            <div>isInCart = {isInCart}</div>
            {productsActions}
        </div>
    );

    const getProducts = async () => {
        const _fileName = "./data.json";
        const res = await fetch(`${_fileName}`);
        const body = await res.json();
        return body;
    };
}

export default Product;
