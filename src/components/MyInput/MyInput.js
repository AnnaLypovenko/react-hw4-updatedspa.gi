import React from "react";

function MyInput(props) {
    const {input, placeholder, meta, type} = props;
    const {error, touched} = meta;
    
    return (
        <div>
            <input {...input} placeholder={placeholder} type={type}/>
            {
                error && touched &&
                    <span className="error">{error}</span>
            }
        </div>
    )
}
export default MyInput;