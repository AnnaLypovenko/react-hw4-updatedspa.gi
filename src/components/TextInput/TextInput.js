import React from 'react';
import {useField} from 'formik';
import "./TextInput.css";


const TextInput = ({label, ...props}) => {
    const [field, meta] = useField(props);
    const classes = meta.touched && meta.error ?
        "text-input form-control is-invalid"
        : "text-input form-control";

    const labelMarkup = label
        ? <label htmlFor={props.id || props.name}>{label}</label>
        : null;

    return (
        <>
            <div className="form-group col-12 text-left">
                {labelMarkup}
                <input className={classes} {...field} {...props} />
                {
                    meta.touched && meta.error
                        ? (<div className="error invalid-feedback text-left">{meta.error}</div>)
                        : null
                }
            </div>
        </>
    )
};


export default TextInput;