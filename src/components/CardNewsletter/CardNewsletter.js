import React from 'react';
import {makeStyles, StylesProvider} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import styles from './CardNewsletter.module.scss';

function CardNewsletter() {
    return (
        <StylesProvider injectFirst>
            <Card className={styles.newsletter} variant="outlined">
                <CardContent>
                    <Typography className={styles.title}  variant="h5" component="h2">
                        NEWSLETTER
                    </Typography>
                    <Typography className={styles.pos}>
                        <hr className={styles.smallHr}/>
                    </Typography>
                    <Typography className={styles.text} variant="body2" component="p">
                        SIGN UP TO GET EXCLUSIVE<br/>
                        OFFERS FROM YOUR FAVORITE<br/>
                        BRANDS.
                        <br/>
                    </Typography>
                    <form  noValidate autoComplete="off">
                        <TextField className={styles.email} id="outlined-basic" label="Enter your email address" variant="outlined"/>
                    </form>
                </CardContent>
                <CardActions>
                    <Button className={styles.signUpButton} size="big" variant="outlined">SIGN UP</Button>
                </CardActions>
            </Card>
        </StylesProvider>
    );
}

export default CardNewsletter;