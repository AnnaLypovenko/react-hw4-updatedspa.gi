import React from "react";
import {NavLink} from "react-router-dom";
import Logo from "../Logo/Logo"

const Header = () => {

    return (
        <header>
            <Logo/>
            <ul>
                <li>
                    <NavLink to="/">List</NavLink>
                </li>
                <li>
                    <NavLink to="/cart">Cart</NavLink>
                </li>
                <li>
                    <NavLink to="/favorites">Favorites</NavLink>
                </li>
            </ul>
        </header>
    )
};

export default Header;