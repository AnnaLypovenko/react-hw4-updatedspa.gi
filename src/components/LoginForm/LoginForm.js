import React from 'react';
// import { Form, Formik } from 'formik';
// import * as Yup from 'yup';
// import TextInput from '../TextInput/TextInput';
import Checkbox from '../Checkbox/Checkbox.js';
import {Typography, Grid, Paper, TextField, Button} from '@material-ui/core';
import styles from 'react-css-modules';
import './LoginForm.module.scss';

const dataLogin = {
    header: 'registered customer',
    text: 'If you have an account, please log in.',
    buttonText: 'Login'
};

const LoginForm = (props) => {
    // const {dataLogin} = props;
    const {header, text, buttonText} = dataLogin;

    return (
        <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
                    <Typography className={styles.header} variant="h4" gutterBottom>{header}</Typography>
                    <Typography variant="subtitle1" gutterBotto>{text}</Typography>
                    <TextField label="enter your e-mail" style={{marginTop: 20}} fullWidth variant="outlined"/>
                    <TextField label="enter your password" style={{marginTop: 20}}  fullWidth variant="outlined"/>
                    <Checkbox/>
                    <br/>
                    <Button variant="outlined">{buttonText}</Button>
            </Grid>
        </Grid>
    );
};

export default LoginForm;