import React from 'react';
import styles from './Logo.module.scss';
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import {StylesProvider} from '@material-ui/core/styles';

function Logo() {
    return (
        <StylesProvider injectFirst>
            <Box>
                <Typography variant="h2" className={styles.logo}>VIGO SHOP</Typography>
            </Box>
        </StylesProvider>
    );
}

export default Logo;
