import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from 'react-redux'
import {
    BrowserRouter as Router,
} from "react-router-dom";
import styles from "react-styleable";

ReactDOM.render(
  <React.StrictMode>
      <Router>
    <App />
      </Router>
  </React.StrictMode>,

  document.getElementById('root')
);
